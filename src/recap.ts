const myName = 'Raymond';
const myAge = 12;
const suma = (a: number, b: number) => a + b;
suma(2, 2);

class Persona {
  constructor(
    private age: number,
    private name: string,
  ) {}

  getSummary() {
    return `my name is ${this.name}, ${this.age}`;
  }
}

const raymond = new Persona(15, 'Raymond');
raymond.getSummary();
